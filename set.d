// Copyright (c) 2013-2014, Raphaël JAKSE <raphael.jakse@gmail.com>
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

module set;
import std.typecons;
import std.conv;
import std.traits;
// import json;

class Element(T) {
    this(T e) {
        this.e = e;
    }

    this(Element!T e) {
        this.e = e.e;
    }

    override string toString () {
        return to!string(e);
    }

    override hash_t toHash() {
        return typeid(this.e).getHash(&this.e);
    }

    override bool opEquals(Object o) {
        auto O = cast(Element!T) o;
        return O && e == O.e;
    }

    bool opEquals(T o) {
        return e == o;
    }

    override int opCmp(Object o) {
        auto O = cast(Element!T) o;
        return O
            ? (e == O.e
                ? 0
                : (e < O.e
                    ? 1
                    : -1
                )
            )
            : -1;
    }

    int opCmp(T o) {
        return (e == o
            ? 0
            : (e < o
                ? 1
                : -1
            )
        );
    }

    T e;
}

class Set(T = Object) {
    T[T] container;

/** PART I : Basic operations */

    // checks the existence of an object in the set.
    bool contains(TY)(TY o) {
        static if (is(CommonType!(T, TY) == T))
            return (o in container) !is null;
        else
            return ((new Element!TY(o)) in container) !is null;
    }

    // returns the address of the element if it is found, null otherwise.
    T* addressOf(TY)(TY o)
    {
        static if (is(CommonType!(T, TY) == T))
            return o in container;
        else
            return new Element!TY(o) in container;
    }

    // add an element in the set if not already there (in such case, we do nothing)
    void add(TY)(TY e)
    {
        static if (is(CommonType!(T, TY) == T)) {
            container[e] = e;
        } else {
            auto e2 = new Element!TY(e);
            container[e2] = e2;
        }
    }

    // remove the element from the set. returns true on success, false otherwise
    bool remove(TY)(TY e)
    {
        static if (!is(CommonType!(T, TY) == T))
            auto e2 = new Element!TY(e);
        else
            auto e2 = e;

        try {
            container.remove(e2);
            return true;
        } catch (Exception E) {
            return false;
        }
    }

    size_t card() {
        return container.length;
    }

/** PART II: More complicated things */

    // returns the Cartesian product of this set and s2
    //FIXME: check correctness of template related stuffs.
    Set!(TS) CrossImpl(TT, TS = Tuple!(T,TT))(Set!TT s2) {
        auto c = new Set!TS; // set of tuples
        foreach (e1 ; getArray()) {
            foreach (e2 ; s2.getArray()) {
                c.add(tuple(e1, e2));
            }
        }
        return c;
    }

    S Cross(S, TT)(Set!TT s2) {
        return CrossImpl!(TT, S)(s2);
    }

    Set!(Tuple!(T,TT)) Cross(TT)(Set!TT s2) {
        return CrossImpl!(TT, Tuple!(T,TT))(s2);
    }

    // returns the difference between this set and s2
    // see also: minusInPlace
    Set!Type Minus(Type = T, T2)(Set!T2 s2) {
        auto c = new Set!Type;
        foreach (e ; container) {
            if (!s2.contains(e))
                c.add(e);
        }
        return c;
    }

    // removes each elements of s2 in this set
    // see also: minus
    void MinusInPlace(T2 = T)(Set!T2 s2) {
        foreach (k ; s2.container) {
            remove(k);
        }
    }

    // returns the union of this set and s2
    Set!Type Union(Type = T, T2 = T)(Set!T2 s2) {
        auto c = new Set!(T);
        foreach (e ; container) {
                c.add(e);
        }

        foreach (e ; s2.container) {
                c.add(e);
        }
        return c;
    }

    Set!T2 plus(T2 = T, S...)(S elements)
    {
        auto E = new Set!T2;
        foreach (e ; container) {
            E.add(e);
        }

        foreach (e ; elements) {
            E.add(e);
        }
        return E;
    }

    Set!T2 minusElement(T2 = T, TE)(TE element) {
        auto E = new Set!T2;
        foreach (e ; container) {
            if (e != element)
                E.add(e);
        }
        return E;
    }

    // this set becomes the union of this set and s2
    void UnionInPlace(T2 = T)(Set!T2 s2) {
        foreach (e ; s2.container) {
            add(e);
        }
    }

    // returns the union of this set and s2
    Set!(T) Inter(Set!(T) s2) {
        auto c = new Set!(T);
        foreach (e; container) {
//             writeln(typeid(e), " ", e, " ", s2.contains!(Element!T)(new Element!T(e)));
//             writeln(e in s2.container);
            if (s2.contains(e))
                c.add(e);
        }
        return c;
    }

    // this set becomes the union of this set and s2
    void InterInPlace(T2 = T)(Set!T2 s2)
    {
        foreach (e ; container.values) {
            if (!s2.contains(e))
                remove(e);
        }
    }

    Set!Type SymDiff(Type = T, T2 = T)(Set!T2 s2) {
            auto r = new Set!(Type);

            foreach (e ; container) {
                if (!s2.contains(e)) {
                    r.add(e);
                }
            }

            foreach (e ; s2.container) {
                if (!contains(e)) {
                    r.add(e);
                }
            }

            return r;
    }

    void SymDiffInPlace(T2 = T)(Set!T2 s2) {
        foreach (e ; container.values) {
            if (s2.contains(e)) {
                remove(e);
            }
        }

        foreach (e ; s2.container) {
            if (!contains(e)) {
                add(e);
            }
        }
    }

    bool isSubsetOf(T2)(Set!T2 s2, bool strict=false)
    {
        foreach (e; container) {
            if (!s2.contains(e))
                return false;
        }

        if (!strict || s2.card() > card())
            return true;
        else
            return false;
    }


    /** PART III: utilities methods */

    // returns an array of the elements of the set. The type of the keys of this
    // array is integer
    T[] getList() {
        return container.values;
    }

    // returns an array of the elements of the set. The type of the keys of this
    // array is not determined: returning T[T] is not guaranteed in future versions.
    T[T] getArray() {
        return container;
    }

    Set!T copy() {
        auto set = new Set!T;
        set.UnionInPlace(this);
        return set;
    }

//    void parse(S)(S s)
//    in {
//       assert(s.length > 1 && s.front == '{');
//    }
//    body {
//       s.popFront();
//       while(!s.empty)
//       {
//          static if (is(Unqual!T == Object)) {
//             if (s.front == '"') {
//                 s2 = json.parse!string(s1);
//                if (s.front == 'd') {
//                   add(to!dstring(s2));
//                   s.popFront();
//                }
//                else if (s.front == 'w') {
//                   add(to!wstring(s2));
//                   s.popFront();
//                }
//                else if (s.front == 'c') {
//                   add(to!string(s2));
//                   s.popFront();
//                }
//                else
//                   add(s2);
//             }
//             else if (s.front == '{')
//             {
//                add(parse!(Set!Object)(s));
//             }
//             else
//             {
//                add(parse!double(s)); // may throw an exception
//             }
//          }
//          else static if (Unqual!T == Set) {
//             auto S = new T;
//             S.parse(s);
//             add(S);
//          }
//          else
//             add(parse!T());
// 
//          munch(" \t\n\r");
//          if (s.front != ',')
//             throw new ConvException(text("Comma expected"));
//          munch(" \t\n\r");
//          if (s.front == '}')
//             break;
//       }
//       assert(!s.empty);
//       s.popFront();
//    }

    /** PART IV: D stuffs */
    override string toString () {
        string ret = "{";
        foreach (e; container) {
            if (ret.length > 1)
                ret ~= ", " ~ to!string(e);
            else
                ret ~= to!string(e);
        }
        return ret ~ "}";
    }

    override @trusted hash_t toHash() {
        //NOTE: the hashing operator should be associative since sets are unordered
        try {
            hash_t h = 0;
            foreach (ref e ; container)
                h += typeid(e).getHash(&e);
            return h;
        }
        catch (Exception e) {
            return 0;
        }
    }

    override bool opEquals(Object o) {
        Set!T O = cast(Set!T) o;
        return O && isSubsetOf(O) && card() == O.card();
    }

    override int opCmp(Object o) {
        Set!T O = cast(Set!T) o;
        return O ? (
               (card() - O.card())
            || (toHash() - O.toHash())
            || (firstDifference(getList(), O.getList()))
        ) : -1;
    }

    int opApply ( int delegate ( ref T x ) dg ) {
       return container.opApply(dg);
    }

    this() {}

    this(T[] args) {
        foreach (arg; args) {
            container[arg] = arg;
        }
    }

    this(T[T] args) {
        foreach (arg; args) {
            if (!contains(arg))
                container[arg] = arg;
        }
    }
}

private hash_t hashMin(T)(T[] a, ref size_t count, hash_t min_min, out bool found) {
    if (a.length == 0) {
        found = false;
        return 0;
    }

    hash_t h, curMin = 0;
    bool curMinUnassigned = true;
    size_t newCount = 0;
    for (int i=0; i < a.length ; ++i) {
        h = typeid(a[i]).getHash(&a[i]);
        if (h == min_min) {
            if (newCount == count + 1) {
                ++count;
                found = true;
                return h;
            } else {
                ++newCount;
            }
        } else if (h > min_min && h < curMin || curMinUnassigned) {
            curMin = h;
            curMinUnassigned = false;
        }
    }

    if (curMin > min_min) {
        found = true;
        count = 0;
        return curMin;
    }

    found = false;
    return 0;
}
//FIXME : put in a separated module
hash_t firstDifference(T)(T[] a1, T[] a2)
{
    hash_t min1   = 0,
           min2   = 0;
    size_t count1 = 0,
           count2 = 0;
    bool found;

    for (size_t i=0; i < a1.length; ++i) {
        min1 = hashMin(a1, count1, min1, found);
        min2 = hashMin(a2, count2, min2, found);
        if (!found)
            return min1;
        if (min1 != min2)
            return min1-min2;
    }

    min2 = hashMin(a2, count2, min2, found);
    return found ? min2 : 0;
}

@property Set!(Set!T2) Powerset(T, T2 = T)(Set!T S)
{
    if (S.card() == 0) {
        auto P = new Set!(Set!T2);
        P.add(new Set!T);
        return P;
    } else {
        T lastE;
        auto Complement = new Set!T;

        foreach (e ; S) {
            Complement.add(e);
            lastE = e;
        }

        Complement.remove(lastE);
        Set!(Set!T) PCompl = Powerset!(T, T)(Complement);

        auto U = new Set!(Set!T);

        foreach (underset ; PCompl) {
            U.add(underset.plus(lastE));
        }

        return PCompl.Union(U);
    }
}