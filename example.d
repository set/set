import std.stdio;
import std.conv;
import set;

void main() {
    auto S = new Set!();
    S.add("hello");
    S.add(42);
    writeln(S);
    writeln("has 42? ", S.contains(42));
    writeln("has \"42\"? ", S.contains("42"));
    writeln("has \"hello\"? ", S.contains("hello"));
    S.remove("hello");
    writeln(S);
    writeln("has \"hello\"? ", S.contains("hello"));
    writeln("address of 42 : ", S.addressOf(42));

    auto E = new Set!int([1,2,3,4]); // it is possible to declare an empty set
    auto F = new Set!int([3,4,5,6]); // of int like this: auto G = new Set!int;

    writeln("union: ", E.Union(F));
    writeln("inter: ", E.Inter(F));
    writeln("symmetric difference: ", E.SymDiff(F));
    writeln("minus: ", E.Minus(F));
    writeln("cross: ", E.Cross(F));
    writeln("Powerset: ", E.Powerset);

    S.UnionInPlace(E);
    writeln("Union in place: ", S);

    // lets iterate over elements:
    foreach (element ; S) {
        write(element);

        // beware, because S is an untyped set (Set!() or Set!Object),
        // type of element is Element.
        // If you want the real value, cast to Element!the_type_of_your_element
        // and access to the e property of the class.

        auto el = cast(Element!int)(element);
        write(" (", el.e, ") ");

        // el.e is an int.
        // Note that this works only because the only remaining values in S
        // are integer values. If another type were present in the set, we
        // would experience a segmentation fault.
    }
    writeln();
}